angular.module('pikeClient.controllersUsers', ['firebase'])

    .controller('ConnexionCtrl', function($scope, $state, $ionicPopup) {

        $scope.validerConnexion = function () {
            var email = document.getElementById('email_co').value;
            var mdp = document.getElementById('mdp_co').value;
            firebase.auth().signInWithEmailAndPassword(email, mdp).then(function() {
                $scope.connecte = true;
            }, function(error) {

                switch(error.code) {
                    case "auth/user-disabled":
                        var alertPopup = $ionicPopup.alert({
                            title: 'D�sol�',
                            template: 'Ce compte a �t� d�sactiv�.'
                        });
                        break;
                    case "auth/invalid-email":
                        var alertPopup = $ionicPopup.alert({
                            title: 'D�sol�',
                            template: 'L\'adresse email n\'est pas valide.'
                        });
                        break;
                    case "auth/user-not-found":
                        var alertPopup = $ionicPopup.alert({
                            title: 'D�sol�',
                            template: 'L\'email est inconnu.'
                        });

                        break;
                    case "auth/wrong-password":
                        var alertPopup = $ionicPopup.alert({
                            title: 'D�sol�',
                            template: 'Le mot de passe est incorrecte.'
                        });
                        break;
                }
            });
            return false;
        };

        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                $scope.connecte = true;
                $scope.$apply();
            }
            else {
                $scope.connecte = false;
                $scope.$apply();
            }
        });

        $scope.deconnexion = function() {
            firebase.auth().signOut().then(function() {
                $scope.connecte = false;
            }, function(error) {
                var alertPopup = $ionicPopup.alert({
                    title: 'D�sol�',
                    template: 'Une erreur s\'est produite lors du traitement de votre demande'
                });
            });
            return false;
        };

    })

    .controller('InscriptionCtrl', function($scope, $ionicPopup, $location, $state, Voitures, Couleurs) {

        $scope.voitures = Voitures.get();
        $scope.couleurs = Couleurs.get();


        function writeUserData(userId, userPrenom, userVoiture, voitureCouleur) {
            firebase.database().ref('utilisateur/' + userId).set({
                prenom : userPrenom,
                voiture : userVoiture,
                couleur : voitureCouleur
            });
        }

        $scope.validerInscription = function() {
            var email_inscription = document.getElementById('email_inscription').value;
            var mdp_inscription = document.getElementById('mdp_inscription').value;
            firebase.auth().createUserWithEmailAndPassword(email_inscription, mdp_inscription).then(function() {
                var userId = firebase.auth().currentUser.uid;
                var liste_v = document.getElementById("liste_voiture");
                var texte_v = liste_v.options[liste_v.selectedIndex].text;
                var liste_c = document.getElementById("liste_couleur");
                var texte_c = liste_c.options[liste_c.selectedIndex].text;
                var prenom = document.getElementById("prenom").value;
                if (userId != null && texte_v != "" && texte_c != "" && prenom != "") {
                    writeUserData(userId, prenom, texte_v, texte_c);
                    $state.go('inscription-reussie');
                }
                else {
                    var user = firebase.auth().currentUser;

                    user.delete().then(function() {
                        // User deleted.
                        var alertPopup = $ionicPopup.alert({
                            title: 'Erreur',
                            template: 'Veuillez remplir tous les champs.'
                        });
                    }, function(error) {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Erreur',
                            template: 'user.delete() n\'a pas fonctionn�'
                        });
                    });
                }
            }, function(error) {
                switch(error.code) {
                    case "auth/email-already-in-use":
                        var alertPopup = $ionicPopup.alert({
                            title: 'D�sol�',
                            template: 'Il existe d�j� un compte avec cet email.'
                        });
                        break;
                    case "auth/invalid-email":
                        var alertPopup = $ionicPopup.alert({
                            title: 'D�sol�',
                            template: 'L\'adresse email n\'est pas valide.'
                        });
                        break;
                    case "auth/operation-not-allowed":
                        var alertPopup = $ionicPopup.alert({
                            title: 'Erreur',
                            template: 'Veuillez activer la connexion email/mdp dans firebase.'
                        });

                        break;
                    case "auth/weak-password":
                        var alertPopup = $ionicPopup.alert({
                            title: 'D�sol�',
                            template: 'Votre mot de passe doit contenir au moins 6 caract�res.'
                        });
                        break;
                }

            });
            return false;
        };

    })

    .controller('InscriptionReussieCtrl', function($scope, $state) {

        $scope.deconnexion = function() {
            firebase.auth().signOut().then(function() {
                $state.go('accueil');
            }, function(error) {
                var alertPopup = $ionicPopup.alert({
                    title: 'D�sol�',
                    template: 'Une erreur s\'est produite lors du traitement de votre demande'
                });
            });
            return false;
        };
    })

    .controller('MdpOublieCtrl', function($scope, $state, $ionicPopup) {

        $scope.mdpOublie = function() {
            var auth = firebase.auth();
            var emailAddress = document.getElementById('email_mdpo').value;

            auth.sendPasswordResetEmail(emailAddress).then(function() {
                // Email sent.
                var alertPopup = $ionicPopup.alert({
                    title: 'Succ�s',
                    template: 'Votre nouveau mot de passe vous a �t� envoy� sur votre adresse email'
                });
            }, function(error) {
                // An error happened.
                var alertPopup = $ionicPopup.alert({
                    title: 'D�sol�',
                    template: 'Une erreur s\'est produite lors du traitement de votre demande'
                });
            });


            return false;
        };
    })

    .controller('DidacticielCtrl', function($scope, $state, $ionicLoading, $cordovaGeolocation) {
        console.log("here");
        var options = {timeout: 10000, enableHighAccuracy: true};


        $cordovaGeolocation.getCurrentPosition(options).then(function(position){

            var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

            var mapOptions = {
                center: latLng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

        }, function(error){
            console.log("Could not get location");
        });
    })
