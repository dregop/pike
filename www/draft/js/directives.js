angular.module('pikeClient.directives', [])

.directive('map', function() {
  return {
    restrict: 'E',
    controller: function ($scope, $element) {


      var rootRef = firebase.database().ref();
      /* global Firebase, google, $, _, GeoFire */


      $scope.parking = {

        radiusInKm: 0.3,

//      center: [37.78565219391501, -122.4058404513338], // roughly 22 4th St. in SF, Firebase HQ 3.0, high density of garages
        center: [48.691889, 6.169837],
        markers: {},
        lines: {},
        map: undefined,
        previousInfoWindow: undefined,
        ref: undefined,
        geoFireGarages: undefined,
        geoQueryGarages: undefined,
        geoFireStreets: undefined,
        geoQueryStreets: undefined,
        $container: undefined,
        /**
         * init
         *
         * initialize the module
         */

        init: function() {
          this.$container = $('.parking-opendata-example');
  //        if (this.$container.length) {
            this.ref = rootRef;
            this.initGaragesAndStreets();
            this.initMap();
     //     }
        },


        /**
         * initGaragesAndStreets
         *
         * create GeoFire objects for garages and streets
         */

        initGaragesAndStreets: function() {
          this.geoFireGarages = new GeoFire(this.ref.child('RuesNancy/garages/_geofire'));
          this.geoQueryGarages = this.geoFireGarages.query({
            center: this.center,
            radius: this.radiusInKm
          });

          this.geoFireStreets = new GeoFire(this.ref.child('RuesNancy/streets/_geofire'));
          this.geoQueryStreets = this.geoFireStreets.query({
            center: this.center,
            radius: this.radiusInKm
          });

          this.initGeoQueryGarages();
          this.initGeoQueryStreets();
        },


        /**
         * initMap
         *
         * draw the map and enable dragging window
         */

        initMap: function() {
          var self = this;
          var initialLatLng = new google.maps.LatLng(this.center[0], this.center[1]);
          var circleOptions = {};
          var circle;


          // INITIALIZE MAP
          this.map = new google.maps.Map(document.getElementById('map-canvas'), {zoom: 16});
          this.map.setCenter(initialLatLng);
          this.map.setTilt(45);

          var marker = new google.maps.Marker({
            position: initialLatLng,
            map: self.map,
            icon: 'img/car.png'
          });

          // DRAW CIRCLE ON MAP
          circleOptions = {
            strokeColor: '#000000',
            strokeOpacity: 0,
            strokeWeight: 1,
            fillColor: '#000000',
            fillOpacity: 0,
            map: self.map,
            center: initialLatLng,
            radius: ((self.radiusInKm) * 1000),
            draggable: false
          };
          circle = new google.maps.Circle(circleOptions);

          // ACTUALISER POSITION CARTE ET CERLCLE en fonction de la geolocalisation du device
          var getLocation = function() {
            if (typeof navigator !== "undefined" && typeof navigator.geolocation !== "undefined") {
              console.log("Asking user to get their location");
              navigator.geolocation.watchPosition(geolocationCallback,  null,
                  {enableHighAccuracy:true});
            } else {
              console.log("Your browser does not support the HTML5 Geolocation API, so this demo will not work.")
            }
          };

          /* Callback method from the geolocation API which receives the current user's location */
          var geolocationCallback = function(location) {
            var LatLng = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);
            self.map.panTo(LatLng);

            // actualise la position du cercle et du marker
            marker.setPosition(LatLng);
            circle.setCenter(LatLng);

            var coord = circle.getCenter();

            self.geoQueryStreets.updateCriteria({
              center: [coord.lat(), coord.lng()],
              radius: self.radiusInKm
            });

            self.geoQueryGarages.updateCriteria({
              center: [coord.lat(), coord.lng()],
              radius: self.radiusInKm
            });
          }
          getLocation();
        },


        /**
         * initGeoQueryGarages
         *
         * set listeners on geoQueryGarages, including click handlers and map drawers
         */

        initGeoQueryGarages: function() {
          var self = this;

          this.geoQueryGarages.on('key_entered', function (id, location) {
            self.ref.child('RuesNancy/garages/').child(id).once('value', function (snapshot) {
              var parking = snapshot.val();
              var points = parking.points;
              var marker;

              if (points.length !== 4) {
                if (!self.markers[points.toString()]) {
                  marker = new google.maps.Marker({
                    position: new google.maps.LatLng(points[0], points[1]),
                    map: self.map
                  });

                  google.maps.event.addListener(marker, 'click', function () {
                    self.displayMapElemInfo(parking, points, 'marker', marker);
                  });

                  self.markers[points.toString()] = marker;
                } else {
                  self.markers[points.toString()].setMap(self.map);
                }
              }
            });
          });

          this.geoQueryGarages.on('key_exited', function (id, location) {
            self.ref.child('garages/').child(id).once('value', function (snapshot) {
              self.markers[snapshot.val().points.toString()].setMap(null);
            });
          });

        },


        /**
         * initGeoQueryStreets
         *
         * set listeners on geoQueryStreets, including click handlers and map drawers
         */

        initGeoQueryStreets: function() {
          var self = this;
          this.geoQueryStreets.on('key_entered', function (id, location) {
            self.ref.child('RuesNancy/streets/').child(id).once('value', function (snapshot) {
              var parking = snapshot.val();
              var points = parking.points;
              var avgPrice = 0;
              var coordinates = [];
              var path;

              if (!self.lines[points.toString()]) {
                coordinates = [
                  new google.maps.LatLng(points[0], points[1]),
                  new google.maps.LatLng(points[2], points[3])
                ];

                _.forEach(parking.rates, function(rate){
                  avgPrice = avgPrice + parseFloat(rate.RATE);
                });

                avgPrice = avgPrice / parking.rates.length;
                avgPrice = Math.floor(avgPrice);

                path = new google.maps.Polyline({
                  path: coordinates,
                  geodesic: true,
                  strokeColor: ['#1CC928', '#F79839', '#F76239'][avgPrice],
                  strokeOpacity: 1.0,
                  strokeWeight: 3
                });

                self.lines[points.toString()] = path;

                google.maps.event.addListener(path, 'click', function () {
                  self.displayMapElemInfo(parking, points, 'path');
                });

                path.setMap(self.map);
              } else {
                self.lines[points.toString()].setMap(self.map);
              }
            });
          });

          this.geoQueryStreets.on('key_exited', function (id, location) {
            self.ref.child('RuesNancy/streets/').child(id).once('value', function (snapshot) {
              self.lines[snapshot.val().points.toString()].setMap(null);
            });
          });
        },

        /**
         * displayMapElemInfo
         *
         * display information about garage or street
         * @param {Object} parking - information about parking from Firebase
         * @param {Array} points - if (type === 'path'), array of points describing a path
         * @param {String} type - 'path' or 'marker'
         * @param {Google Maps Marker} marker - if (type === 'marker'), the Google Maps Marker
         */

        displayMapElemInfo: function(parking, points, type, marker) {
          var content = '<h2 style="width: 100%; margin: 0px; padding: 0px; float: left; clear: both;">' + parking.friendlyName + '</h2>';
          var point;
          var infoWindow;

          _.forEach(parking.rates, function(rate){
            if (rate.BEG && rate.END) {
              content = content + rate.BEG + ' - ' + rate.END;
            } else {
              content = content +rate.DESC;
            }

            if (rate.RATE !== '0') {
              content = content + '<b>: $' + rate.RATE + '</b><br />';
            } else {
              content = content + '<b>: Free</b><br />';
            }
          });

          infoWindow = new google.maps.InfoWindow({
            content: content
          });

          if (this.previousInfoWindow) {
            this.previousInfoWindow.close();
          }

          if (type === 'path') {
            point = new google.maps.LatLng((points[0] + points[2]) / 2, (points[1] + points[3]) / 2);
            infoWindow.open(this.map);
            infoWindow.setPosition(point);
          } else if (type === 'marker') {
            infoWindow.open(this.map, marker);
          }

          this.previousInfoWindow = infoWindow;
        }
      };

      $scope.parking.init();



    //  $scope.onCreate({map: map});


      // Stop the side bar from dragging when mousedown/tapdown on the map
      google.maps.event.addDomListener($element[0], 'mousedown', function (e) {
        e.preventDefault();
        return false;
      });
    }
  }
});

// .directive('changeMode', function() {
//   return {
//     restrict: 'C',
//     templateUrl: 'templates/change-mode.html',
//     scope: {
//       userMode: '=',
//       userState: '='
//     },
//     link: function(scope) {
//       console.log(userMode);
//       if (userMode === 'leaver') {
//         var toMode = "Park";
//         var allowed = (userState === 'parked' || userState === 'driving');
//         var buttonStyle = "positive";
//       } else if (userMode === 'parker') {
//         var toMode = "Leave";
//         var allowed = (userState === 'cruising' || userState === 'parked');
//         var buttonStyle = "royal";
//       }
//     }
//   }
// });
