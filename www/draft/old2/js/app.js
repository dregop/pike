window.onerror = function(msg, url, linenumber) {
    alert('Error message: '+msg+'\nURL: '+url+'\nLine Number: '+linenumber);
    return true;
}

angular.module('pikeClient', ['ionic','ionic.service.core', 'pikeClient.controllers','pikeClient.controllersUsers','pikeClient.controllersTemplate', 'pikeClient.directives', 'pikeClient.services','pikeClient.servicesUsers','pikeClient.servicesTemplate', 'openfb'])



.run(function ($rootScope, $state, $ionicPlatform, $window, OpenFB) {
//alert (window.location.href) ;
//alert ("here")
      if (window.location.href.startsWith("http://localhost") )
      {
        OpenFB.init('800093266801745', 'http://localhost:8100/oauthcallback.html');
      }
// OpenFB.init('800093266801745', 'https://pike.firebaseapp.com/oauthcallback.html');

      else
      if (window.location.href.startsWith("https://pike") )
      {
        OpenFB.init('800093266801745', 'https://pike.firebaseapp.com/oauthcallback.html');
      }
      else OpenFB.init('800093266801745', "https://pike.firebaseapp.com/oauthcallback.html" );

 
  $ionicPlatform.ready(function () {
    console.log('Platform ready');
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
/*
  $rootScope.$on('$stateChangeStart', function(event, toState) {

      if (toState.name !== "tab.didacticiel" && toState.name !== "login" && toState.name !== "intro" && toState.name !== "phone" && toState.name !== "logout" && !$window.sessionStorage['fbtoken']) {
     // event.preventDefault();
      $state.go('intro');
    } else if ((toState.name === "login" || toState.name === "intro") && $window.sessionStorage['fbtoken']) {
      event.preventDefault();
      $state.go('intro');
    }

    if (toState.name !== "app.leave") {
      $rootScope.displayPin = false;
    }
  });
*/

  $rootScope.displayPin = false;

  $rootScope.$on('OAuthException', function() {
    $state.go('login');
  });

        // Create custom defaultStyle.
        function getDefaultStyle() {
            return "" +
                ".material-background-nav-bar { " +
                "   background-color        : " + appPrimaryColor + " !important; " +
                "   border-style            : none;" +
                "}" +
                ".md-primary-color {" +
                "   color                     : " + appPrimaryColor + " !important;" +
                "}";
        }// End create custom defaultStyle

        // Create custom style for product view.
        function getProductStyle() {
            return "" +
                ".material-background-nav-bar { " +
                "   background-color        : " + appPrimaryColor + " !important;" +
                "   border-style            : none;" +
                "   background-image        : url('img/background_cover_pixels.png') !important;" +
                "   background-size         : initial !important;" +
                "}" +
                ".md-primary-color {" +
                "   color                     : " + appPrimaryColor + " !important;" +
                "}";
        }// End create custom style for product view.

        // Create custom style for contract us view.
        function getContractUsStyle() {
            return "" +
                ".material-background-nav-bar { " +
                "   background-color        : transparent !important;" +
                "   border-style            : none;" +
                "   background-image        : none !important;" +
                "   background-position-y   : 4px !important;" +
                "   background-size         : initial !important;" +
                "}" +
                ".md-primary-color {" +
                "   color                     : " + appPrimaryColor + " !important;" +
                "}";
        } // End create custom style for contract us view.

        // Create custom style for Social Network view.
        function getSocialNetworkStyle(socialColor) {
            return "" +
                ".material-background-nav-bar {" +
                "   background              : " + socialColor + " !important;" +
                "   border-style            : none;" +
                "} " +
                "md-ink-bar {" +
                "   color                   : " + socialColor + " !important;" +
                "   background              : " + socialColor + " !important;" +
                "}" +
                "md-tab-item {" +
                "   color                   : " + socialColor + " !important;" +
                "}" +
                " md-progress-circular.md-warn .md-inner .md-left .md-half-circle {" +
                "   border-left-color       : " + socialColor + " !important;" +
                "}" +
                " md-progress-circular.md-warn .md-inner .md-left .md-half-circle, md-progress-circular.md-warn .md-inner .md-right .md-half-circle {" +
                "    border-top-color       : " + socialColor + " !important;" +
                "}" +
                " md-progress-circular.md-warn .md-inner .md-gap {" +
                "   border-top-color        : " + socialColor + " !important;" +
                "   border-bottom-color     : " + socialColor + " !important;" +
                "}" +
                "md-progress-circular.md-warn .md-inner .md-right .md-half-circle {" +
                "  border-right-color       : " + socialColor + " !important;" +
                " }" +
                ".spinner-android {" +
                "   stroke                  : " + socialColor + " !important;" +
                "}" +
                ".md-primary-color {" +
                "   color                   : " + socialColor + " !important;" +
                "}" +
                "a.md-button.md-primary, .md-button.md-primary {" +
                "   color                   : " + socialColor + " !important;" +
                "}";
        }// End create custom style for Social Network view.


    })

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider


  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: "AppCtrl"
  })


/*
  .state('intro', {
    url: '/',
    templateUrl: 'templates/intro.html',
    controller: 'IntroCtrl'
  })*/

  .state('phone', {
    url: '/phone',
    templateUrl: 'templates/add-phone.html',
    controller: 'PhoneCtrl'
  })
  .state('login2', {
    url: "/login2",
    templateUrl: "templates/login2.html",
    controller: "Login2Ctrl"
  })
  .state('logout', {
    url: "/logout",
    template: '<div>Logging you out...</div>',
    controller: "LogoutCtrl"
  })
  .state('app.map', {
    url: "/map",
    views: {
      'menuContent': {
        templateUrl: "templates/map.html",
        controller: "MapCtrl"
      }
    }
  })

  .state('app.map2', {
        url: "/map2",
        views: {
          'menuContent': {
            templateUrl: "templates/map2.html",
            controller: "MapCtrl2"
          }
        }
      })


      .state('app.leave', {
    url: "/leave",
    views: {
      'menuContent': {
        templateUrl: "templates/leave.html",
        controller: "LeaveCtrl"
      }
    }
  })
/////////////////////////////////////////////////// Travail Pierre///////////////////////////////////////
      // Each tab has its own nav history stack:
      .state('accueil', {
        url: '/accueil',
        templateUrl: 'templates/tab-accueil.html'
      })

          .state('connexion', {
            url: '/connexion',
                templateUrl: 'templates/tab-connexion.html',
                controller: 'ConnexionCtrl'
           })
          .state('inscription', {
            url: '/inscription',
                     templateUrl: 'templates/tab-inscription.html',
                controller: 'InscriptionCtrl'
           })

          .state('inscription-reussie', {
            url: '/inscription-reussie',
            templateUrl: 'templates/inscription_reussie.html',
            controller: 'InscriptionReussieCtrl'
          })

          .state('mdp_oublie', {
            url: '/mdp_oublie',
            templateUrl: 'templates/mdp_oublie.html',
            controller: 'MdpOublieCtrl'
          })

          .state('didacticiel', {
            url: '/didacticiel',

                templateUrl: 'templates/tab-didacticiel.html',
                controller: 'DidacticielCtrl'
          })





////////////////////////Template taxi

        // Home screen
        .state('home', {
            //cache: false,
            url: '/home',
            templateUrl: 'templates/home.html',
            controller: 'HomeCtrl'
        })

            // Search for a place
            .state('places', {
                url: '/places',
                templateUrl: 'templates/places.html',
                controller: 'PlacesCtrl'
            })

            // Choose payment method
            .state('payment_method', {
                url: '/payment-method',
                templateUrl: 'templates/payment-method.html',
                controller: 'PaymentMethodCtrl'
            })

            // Find a driver
            .state('finding', {
                url: '/finding',
                templateUrl: 'templates/finding.html',
                controller: 'FindingCtrl'
            })

            // Show driver profile
            .state('driver', {
                url: '/driver',
                templateUrl: 'templates/driver.html',
                controller: 'DriverCtrl'
            })

            // Tracking driver position
            .state('tracking', {
                url: '/tracking',
                templateUrl: 'templates/tracking.html',
                controller: 'TrackingCtrl'
            })

            // Show history
            .state('history', {
                url: '/history',
                templateUrl: 'templates/history.html',
                controller: 'HistoryCtrl'
            })

            // Show notifications
            .state('notification', {
                url: '/notification',
                templateUrl: 'templates/notification.html',
                controller: 'NotificationCtrl'
            })

            // Support form
            .state('support', {
                url: '/support',
                templateUrl: 'templates/support.html',
                controller: 'SupportCtrl'
            })

            // Profile page
            .state('profile', {
                url: '/profile',
                templateUrl: 'templates/profile.html',
                controller: 'ProfileCtrl'
            })

            // login screen
            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'Auth2Ctrl'
            })

            // register screen
            .state('register', {
                url: '/register',
                templateUrl: 'templates/register.html',
                controller: 'Auth2Ctrl'
            })












        $urlRouterProvider.otherwise("/accueil");

});