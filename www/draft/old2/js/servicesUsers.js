/**
 * Created by LoiselA on 24/08/2016.
 */
angular.module('pikeClient.servicesUsers', [])

.factory('Voitures', function() {

    var voitures = [
        {model : "Alpha Rom�o", logo : "alpha_romeo_logo.png"},
        {model : "Aston Martin", logo : "aston_martin_logo.png"},
        {model : "Audi", logo : "audi_logo.png"},
        {model : "Citro�n", logo : "citroen_logo.png"},
        {model : "Peugeot", logo : "peugeot_logo.png"}
    ];
    return {

        get: function() {
            return voitures;
        }
    }
})

    .factory('Couleurs', function() {

        var couleurs = [
            {couleur : "Blanc"},
            {couleur : "Noir"},
            {couleur : "Gris"},
            {couleur : "Rouge"},
            {couleur : "Bleu"}
        ];
        return {

            get: function() {
                return couleurs;
            }
        }
    });
