    <ion-nav-buttons side="left">
      <button menu-toggle="left" class="button button-icon icon ion-navicon"></button>
    </ion-nav-buttons>

    <ion-nav-buttons ng-if="userState === 'sweetching' || userState === 'requesting'" side="right">
      <button class="button" ng-click="cancel()">Cancel</button>
    </ion-nav-buttons>

    <ion-nav-buttons ng-if="userMode === 'leaver' && (userState === 'parked' || userState === 'driving')" side="right">
      <button class="button button button-positive" ng-click="parkMode()">Park</button>
    </ion-nav-buttons>

    <ion-nav-buttons ng-if="(userState === 'driving' || userState === 'parked') && userMode === 'parker'" side="right">
      <button class="button button button-royal" ng-click="leaveMode()">Leave</button>
    </ion-nav-buttons>


.controller('MapCtrl', function($scope, $rootScope, $ionicActionSheet, $ionicPopup, $stateParams, $state, $ionicLoading, geolocation, $http, OpenFB, PubNub, User, API, Driver, SweetchModel) {


$scope.leaveMode = function() {
    console.log("Sweetching to Leave mode")
    $scope.userMode = "leaver";
    $scope.userState = "parked";
    $scope.buttonStyle = "royal";
    $scope.requestString = "Leave a spot";
    $rootScope.displayPin = true;
    $scope.spotMarker;
    $scope.driverMarker;
  }

  $scope.parkMode = function() {
    $scope.userMode = "parker";
    $scope.userState = "driving";
    $scope.buttonStyle = "positive";
    $scope.requestString = "Find nearest spot";
    $rootScope.displayPin = false;
  }

  $scope.parker = function() {
    $scope.userMode === "parker";
  }

  $scope.leaver = function() {
    $scope.userMode === "leaver";
  }

  $scope.userStateIdle = function() {
    if ($scope.userMode === "leaver") {
      $rootScope.displayPin = true;
      $scope.userState = "parked";
      if ($scope.driverMarker) {
        $scope.driverMarker.setMap(null);
      }
      if ($scope.spotMarker) {
        $scope.spotMarker.setMap(null);
      }
    } else if ($scope.userMode === "parker") {
      $scope.userState = "driving";
    }
  }

  $scope.userStateRequesting = function() {
    if ($scope.userMode === "leaver") {
      $rootScope.displayPin = false;
    }
    $scope.userState = "requesting";
  }

  $scope.userStateSweetching = function() {
    $scope.userState = "sweetching";
  }

  $scope.userStateDriving = function() {
    $scope.userState = "driving";
  }

  $scope.requestSweetch = function() {
    // Takes user location
    console.log("Requesting a Sweetch");
    $scope.userStateRequesting();

    if ($scope.userMode === "parker") {
      $ionicLoading.show({
        content: 'Contacting drivers (2)...',
        showBackdrop: false
      });

      geolocation.getLocation().then(function(data){
        $scope.coords = {lat:data.coords.latitude, lng:data.coords.longitude};

        console.log($scope.coords);
        requestSweetchInBackend();
      });
    } else if ($scope.userMode === "leaver") {
      $ionicLoading.show({
        content: 'Give us 5 minutes to find a match',
        showBackdrop: false
      });
//      var mapCenter = $scope.map.getCenter();
     // var mapCenter = $scope.parking.getCenter();
      mapCenter={};
      mapCenter.lat=49.4527278;
      mapCenter.lng= 1.0952633;
      // $scope.coords = new google.maps.LatLng($scope.map.getCenter())
 //     $scope.coords = { lat: mapCenter.lat(), lng: mapCenter.lng() };
      $scope.coords = { lat: mapCenter.lat, lng: mapCenter.lng };
      $scope.spotMarker = addMarker($scope.coords);
      requestSweetchInBackend();
    }
  }

  // Cancels the spot request to the backend
  $scope.cancel = function () {
    if ($scope.userState == "sweetching") {
      showCancelOptions();
    } else if ($scope.userState == "requesting") {
      $scope.unsubscribe();
      $scope.cancelSweetchInBackend($scope.sweetch.id);
      $ionicLoading.hide();
      SweetchModel.delete();
      $scope.userStateIdle();
    }
  }

  $scope.confirmSweetch = function () {
    $scope.userStateDriving();
    $scope.spotMarker.setMap(null);
    $scope.driverMarker.setMap(null);
    $scope.alertUser("confirmed");
    confirmInBackend($scope.sweetch.id);
  }

  function requestSweetchInBackend() {
    console.log("Sending request to backend");
    subscribe();
    var params = {
      auth_token: window.sessionStorage['fbtoken']
    }
    if ($scope.userMode === "parker") {
      params.parker_lat = $scope.coords.lat;
      params.parker_lng = $scope.coords.lng;
    } else if ($scope.userMode === "leaver") {
      params.leaver_lat = $scope.coords.lat;
      params.leaver_lng = $scope.coords.lng;
    }
    console.log(params);

    // Post request to create Sweetch
    API.post('/api/v1/sweetches', params).
    success(function(data, status, headers, config) {
      // Display the spot on map if the sweetch is in progress
      // Otherwise wait for a notification through pubnub channel
      console.log(data)
      sweetch = data["sweetch"];
      $scope.sweetch = SweetchModel.new(sweetch);
      SweetchModel.save();
      if (sweetch["state"] == "in_progress") {
        sweetchObject = angular.fromJson(sweetch);

        // Define other driver
        if ($scope.userMode === "parker") {
          $scope.driver = Driver.save({
            facebook_id: sweetchObject.leaver_facebook_id,
            first_name: sweetchObject.leaver_first_name,
            ph: sweetchObject.leaver_ph,
          });
        } else if ($scope.userMode === "leaver") {
          $scope.driver = Driver.save({
            facebook_id: sweetchObject.parker_facebook_id,
            first_name: sweetchObject.parker_first_name,
            ph: sweetchObject.parker_ph,
            eta: sweetchObject.eta
          });
        }

        // Load view with all the elements for Sweetching
        loadSweetchingView();
      }
    }).
    error(function(data, status, headers, config) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });
  }

  function confirmInBackend() {
    params = { auth_token: window.sessionStorage['fbtoken'], state: "validated" };
    API.post('/api/v1/sweetches/' + $scope.sweetch.id, params).
    success(function(data, status, headers, config) {
      console.log("API: Validated Sweetch");
    }).
    error(function(data, status, headers, config) {
      console.log("API: Error, could not validate Sweetch");
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });
  };

  function loadSweetchingView() {
    $ionicLoading.hide();
    $scope.userStateSweetching();
    // Draw route to spot
    if ($scope.userMode === "parker") {
      $scope.spotLocation = new google.maps.LatLng($scope.sweetch.leaver_lat, $scope.sweetch.leaver_lng);
      $scope.drawRouteToSpot($scope.userLocation, $scope.spotLocation);
      $state.reload();
    } else if ($scope.userMode === "leaver") {
      $scope.parkerLocation = new google.maps.LatLng($scope.sweetch.parker_lat, $scope.sweetch.parker_lng);
      setDriverMarker($scope.parkerLocation);
    }
  }

  function subscribe() {
	  console.log("myChannel " +$scope.myChannel) ;
    PubNub.ngSubscribe({
        channel: $scope.myChannel,
        message: function (m) { notificationReceived(m[0]) }
    });
  }

  //***** Received notification *****//
  function notificationReceived(notif) {
    console.log('Notification received: ' + notif.title);
    console.log(notif.data);
    if (notif.title == "Match Found") {
      // Update the Sweetch locally
      if ($scope.userMode === "parker") {
        // Update the Sweetch object
        $scope.sweetch = SweetchModel.update({leaver_lat: notif.data.lat, leaver_lng: notif.data.lng, state: "in_progress"});
      } else if ($scope.userMode === "leaver") {
        // Update the Sweetch object
        $scope.sweetch = SweetchModel.update({parker_lat: notif.data.p_lat, parker_lng: notif.data.p_lng, state: "in_progress"});
      }
      // Set information about the leaver
      $scope.driver = Driver.save(notif.data);
      loadSweetchingView();
    } else if (notif.title == "Match Not Found") {
      $ionicLoading.hide();
      $scope.userStateIdle();
      SweetchModel.delete();
      $scope.alertUser("not_found");
    } else if (notif.title == "Sweetch Validated") {
      if ($scope.userState == "sweetching") {
        $scope.userState = "parked";
        $scope.alertUser("validated");
      }
    } else if (notif.title == "Sweetch Failed") {
      if ($scope.userState == "sweetching") {
        $scope.userStateIdle();
        $scope.alertUser("failed");
      }
    }
  }

  // Triggered on a button click, or some other target
  function showCancelOptions() {
    if ($scope.userMode === "parker") {
      var cancelOptions = [{ text: 'I could not find the car' }, { text: 'I found another spot' }, { text: 'I did not get the spot' }];
    } else if ($scope.userMode === "leaver") {
      var cancelOptions = [{ text: 'I had to leave' }, { text: 'The driver did not come' }];
    }

    $ionicActionSheet.show({
      buttons: cancelOptions,
      titleText: 'Tell us what went wrong',
      cancelText: 'Cancel',
      cancel: function() {},
      buttonClicked: function(index) {
        $scope.userStateIdle();
        $scope.failSweetch(index, $scope.sweetch.id);
        return true;
      },
      destructiveButtonClicked: function() { return true; }
    });
  };

  function addMarker(coords) {
    return new google.maps.Marker({
      position: coords,
      //map: $scope.parking.map,
      title: "Your Spot"
    })
  }

  function setDriverMarker(pos) {
    var driver_name = Driver.get().first_name;
    var infowindow = new google.maps.InfoWindow({
      content: driver_name + ' is here'
    })

    $scope.driverMarker = new google.maps.Marker({
      position: pos,
      map: $scope.map,
      icon: 'img/carPin.png',
      title: 'Your Sweetch buddy',
      visible: true
    })
    infowindow.open($scope.map, $scope.driverMarker);
    google.maps.event.addListener($scope.driverMarker, 'click', function() {
      infowindow.open($scope.map, $scope.driverMarker);
    });
  }

    function init() {
      $scope.parkMode();

    }

    init();

})
