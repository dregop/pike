angular.module('pikeClient.servicesTemplate', [])

.factory('Voitures', function() {

  var voitures = [
      {model : "Alpha Roméo", logo : "alpha_romeo_logo.png"},
      {model : "Aston Martin", logo : "aston_martin_logo.png"},
      {model : "Audi", logo : "audi_logo.png"},
      {model : "Citroën", logo : "citroen_logo.png"},
      {model : "Peugeot", logo : "peugeot_logo.png"}
  ];
  return {

    get: function() {
      return voitures;
    }
  }
})

.factory('Couleurs', function() {

  var couleurs = [
      {couleur : "Blanc"},
      {couleur : "Noir"},
      {couleur : "Gris clair"},
      {couleur : "Gris foncé"},
      {couleur : "Rouge"},
      {couleur : "Bleu"},
      {couleur : "Vert"},
      {couleur : "Jaune"},
      {couleur : "Autre"}
  ];
  return {

    get: function() {
      return couleurs;
    }
  }
})

.factory('Profil', function() {

  function getProfil() {
        var user = firebase.auth().currentUser;
        return user;
  }

  function mdpOublie(emailAddress) {
    var auth = firebase.auth();
    return auth.sendPasswordResetEmail(emailAddress);
  }


  return {
    save: function (userId, userPrenom, userVoiture, voitureCouleur) {
          firebase.database().ref('utilisateur/' + userId).set({
              prenom : userPrenom,
              voiture : userVoiture,
              couleur : voitureCouleur
          })
    },

    get: function() {
      return getProfil();
    },

    "getPrenom": function(userId) {
      return firebase.database().ref('/utilisateur/' + userId);
    },

    "getHistory": function(userId) {
      return firebase.database().ref('/utilisateur/' + userId + '/historique/');
    },

    "inscription": function(email_inscription, mdp_inscription) {
      return firebase.auth().createUserWithEmailAndPassword(email_inscription, mdp_inscription);
    },

    "connexion": function(email, mdp) {
      return firebase.auth().signInWithEmailAndPassword(email, mdp);
    },

    "deconnexion": function() {
      return firebase.auth().signOut();
    },
  }
})

.factory('Places', function () {
  var places = [
    {
      name: "Bach Mai hospital",
      address: "Giai Phong street, Hanoi, Vietnam",
      distance: 1
    },
    {
      name: "Vietnam - France hospital",
      address: "Phuong Mai street, Hanoi, Vietnam",
      distance: 1.2
    },
    {
      name: "Pico plaza",
      address: "No 02 Truong Chinh street, Hanoi, Vietnam",
      distance: 1.3
    },
    {
      name: "Pho Vong",
      address: "Pho Vong street, Hanoi, Vietnam",
      distance: 1.4
    },
    {
      name: "iMobile",
      address: "Pho Vong street, Hanoi, Vietnam",
      distance: 1.5
    }
  ];

  var recent = [
    {
      name: "Pho Vong",
      address: "Pho Vong street, Hanoi, Vietnam",
      distance: 1.4
    },
    {
      name: "iMobile",
      address: "Pho Vong street, Hanoi, Vietnam",
      distance: 1.5
    }
  ];

  return {
    all: function () {
      return places;
    },
    remove: function (post) {
      places.splice(places.indexOf(post), 1);
    },
    get: function (postId) {
      for (var i = 0; i < places.length; i++) {
        if (places[i].id === parseInt(postId)) {
          return places[i];
        }
      }
      return null;
    },
    recent: function () {
      return recent;
    }
  };
})

.factory('Drivers', function () {
  var drivers = [
    {
      id: 1,
      name: "Pierre Dupont",
      plate: "29A578.89",
      brand: "Kia Morning",
      distance: 0.6,
      status: "Bidding"
    },
    {
      id: 2,
      name: "Denis Suarez",
      plate: "29A578.89",
      brand: "Kia Morning",
      distance: 0.6,
      status: "Contacting"
    },
    {
      id: 3,
      name: "Karim Benzema",
      plate: "29A578.89",
      brand: "Kia Morning",
      distance: 0.6,
      status: "Contacting"
    },
    {
      id: 4,
      name: "Martin Montoya",
      plate: "29A578.89",
      brand: "Kia Morning",
      distance: 0.6,
      status: "Contacting"
    },
  ];

  return {
    all: function () {
      return drivers;
    },
    remove: function (driver) {
      drivers.splice(drivers.indexOf(driver), 1);
    },
    get: function (driverId) {
      for (var i = 0; i < drivers.length; i++) {
        if (drivers[i].id === parseInt(driverId)) {
          return drivers[i];
        }
      }
      return null;
    }
  };
})

.factory('Trips', function() {
    var trips = [
      {
        date: '2016-01-02',
        heure: '16h 00',
        avec: 'Marc',
        lieu: [48.69371455, 6.16984455],
        nbre_points_gagnes : '10',
        mode : "driver"
      },
      {
        date: '2016-11-09',
        heure: '8h 00',
        avec: 'Jean',
        lieu: [8.69398055, 6.17098755],
        nbre_points_gagnes : '20',
        mode : "parker"
      }
    ];

    var saveData = function() {

        var postData = {
          date: '2017-01-08',
          heure: '15h 00',
          avec: 'pierro',
          lieu: [48.69371455, 6.16984455],
          nbre_points_gagnes : '20',
          mode : "leaver"
        };


        // Get a key for a new Post.
        var newPostKey = firebase.database().ref('/utilisateur/' + 'QU9wqsO0wAcDdHJ5GS4TfMekHIt2').child('historique').push().key;

        // Write the new post's data simultaneously in the posts list and the user's post list.
        var updates = {};
        //
        updates['/utilisateur/' + 'QU9wqsO0wAcDdHJ5GS4TfMekHIt2' + '/historique/' + newPostKey] = postData;

        firebase.database().ref().update(updates);

    }

    return {
      all: function () {
        return trips;
      },
      save: function () {
        saveData();
      }
    };
})

.factory('Notifications', function () {
  var notifications = [
    {
      id: 1,
      title: "New price from Jan 2016",
      content: "",
      createdAt: "2016-02-14 12:00:00",
      read: true
    },
    {
      id: 2,
      title: "New version 1.1.1",
      content: "",
      createdAt: "2016-02-13 12:00:00",
      read: false
    },
    {
      id: 3,
      title: "New version 1.1.0",
      content: "",
      createdAt: "2016-02-12 12:00:00",
      read: false
    }
  ];

  return {
    all: function () {
      return notifications;
    },
    remove: function (notification) {
      notifications.splice(notifications.indexOf(notification), 1);
    },
    get: function (notificationId) {
      for (var i = 0; i < notifications.length; i++) {
        if (notifications[i].id === parseInt(notificationId)) {
          return notifications[i];
        }
      }
      return null;
    }
  };
})
