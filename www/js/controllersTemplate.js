angular.module('pikeClient.controllersTemplate', ['firebase', 'ngCordova'])

// Home controller
.controller('HomeCtrl', function($scope, $state, $ionicPopup, $timeout, $ionicHistory) {

  var choix = 0;

  // map height
  $scope.mapHeight = 480;

  // show - hide booking form
  $scope.showForm = false;

  // show - hide modal bg
  $scope.showModalBg = false;

  // toggle form
  $scope.toggleForm = function() {
    $scope.showForm = !$scope.showForm;
    $scope.showModalBg = ($scope.showForm == true);
  }

  // list vehicles
  $scope.vehicles = [
    {
      name: 'Je cherche une place',
      icon: 'ion-search',
      active: true
    },
    {
      name: 'Je quitte ma place',
      icon: 'icon-car',
      active: false
    }
  ]

  // Note to driver
  $scope.note = '';

  // Promo code
  $scope.promo = '';

  // toggle active vehicle
  $scope.toggleVehicle = function(index) {
    for (var i = 0; i < $scope.vehicles.length; i++) {
      $scope.vehicles[i].active = (i == index);
      if (index == 1) $state.go('conducteur-trouve');
      else $state.go('place-trouvee');
    }
  }


  // Show note popup when click to 'Notes to driver'
  $scope.showNotePopup = function() {
    $scope.data = {}

    // An elaborate, custom popup
    var myPopup = $ionicPopup.show({
      templateUrl: 'templates/popup-note.html',
      title: 'Notes to driver',
      scope: $scope,
      buttons: [
        { text: 'Cancel' },
        {
          text: '<b>Save</b>',
          type: 'button-balanced',
          onTap: function(e) {
            if (!$scope.data.note) {
              //don't allow the user to close unless he enters note
              e.preventDefault();
            } else {
              return $scope.data.note;
            }
          }
        },
      ]
    });
    myPopup.then(function(res) {
      $scope.note = res;
    });
  };

  // Show promote code popup when click to 'Promote Code'
  $scope.showPromoPopup = function() {
    $scope.data = {}

    // An elaborate, custom popup
    var myPopup = $ionicPopup.show({
      templateUrl: 'templates/popup-promo.html',
      title: 'Promo code',
      scope: $scope,
      buttons: [
        { text: 'Cancel' },
        {
          text: '<b>Save</b>',
          type: 'button-balanced',
          onTap: function(e) {
            if (!$scope.data.promo) {
              //don't allow the user to close unless he enters note
              e.preventDefault();
            } else {
              return $scope.data.promo;
            }
          }
        },
      ]
    });
    myPopup.then(function(res) {
      $scope.promo = res;
    });
  };

  // go to next view when the 'Book' button is clicked
  $scope.book = function() {
    // hide booking form
    $scope.toggleForm();

    // go to finding state
    $state.go('finding');
    //$state.go('tracking');
  }
})

// Places Controller
.controller('PlacesCtrl', function($scope, Places) {
  // set list places
  $scope.places = Places.all();

  // list recent places
  $scope.recentPlaces = Places.recent();
})


// Payment Method Controller
.controller('PaymentMethodCtrl', function($scope, $state) {
  // default value
  $scope.choice = 'A';

  // change payment method
  $scope.changeMethod = function (method) {
    // add your code here

    // return to main state
    $state.go('home');
  }
})

// Finding controller
.controller('FindingCtrl', function($scope, Drivers, $state) {
  // get list of drivers
  $scope.drivers = Drivers.all();

  // start on load
  $scope.init = function() {
    setTimeout(function() {
      $state.go('driver');
    }, 1000)
  }
})

// Driver controller
.controller('placeTrouveeCtrl', function($scope, Drivers, $state, $ionicHistory, $cordovaVibration, $cordovaLaunchNavigator) {

  $scope.mapCreated = function(map, geolocationData) {
    $scope.map = map;
    $scope.geolocationData = geolocationData;
  };

  // get driver profile
  $scope.vibrate = function() {
    $cordovaVibration.vibrate(400);
  }

  $scope.track = function() {
    $state.go('quitter-place');
  };

  // change driver id here
  $scope.driver = Drivers.get(1);

  // go to tracking screen

  $scope.launchNavigator = function() {
    var destination = "Rouen";
  var start = "Nancy";
    $cordovaLaunchNavigator.navigate(destination, start).then(function() {
      console.log("Navigator launched");
    }, function (err) {
      console.error(err);
    });
  };

})

// Tracking controller
.controller('TrackingCtrl', function($scope, Drivers, $state, $ionicHistory, $ionicPopup) {
  // map object
  $scope.map = null;

  // map height
  $scope.mapHeight = 360;

  // get driver profile
  // change driver id here
  $scope.driver = Drivers.get(1);

  // ratings stars
  $scope.stars = [0, 0, 0, 0, 0];

  function initialize() {
    // set up begining position
    var myLatlng = new google.maps.LatLng(21.0227358,105.8194541);

    // set option for map
    var mapOptions = {
      center: myLatlng,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      zoomControl: false,
      streetViewControl: false
    };
    // init map
    var map = new google.maps.Map(document.getElementById("map_tracking"), mapOptions);

    // assign to stop
    $scope.map = map;

    // get ion-view height
    var viewHeight = window.screen.height - 44; // minus nav bar
    // get info block height
    var infoHeight = document.getElementsByClassName('tracking-info')[0].scrollHeight;

    $scope.mapHeight = viewHeight - infoHeight;
  }

  function showRating() {
    $scope.data = {
      stars: $scope.stars
    }

    // An elaborate, custom popup
    var myPopup = $ionicPopup.show({
      templateUrl: 'templates/popup-rating.html',
      title: 'Thank you',
      scope: $scope,
      buttons: [
        { text: 'Cancel' },
        {
          text: '<b>Submit</b>',
          type: 'button-balanced',
          onTap: function(e) {
            if (!$scope.data.stars) {
              //don't allow the user to close unless he enters note
              e.preventDefault();
            } else {
              return $scope.data.stars;
            }
          }
        },
      ]
    });
    myPopup.then(function(res) {
      // save rating here

      // go to home page
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $state.go('home');
    });
  }

  $scope.$on('$ionicView.enter', function() {

  });

  // load map when the ui is loaded
  $scope.init = function() {
    setTimeout(function() {
      initialize();
    }, 200);

    // finish trip
    setTimeout(function() {
      showRating();
    }, 1000)
  }
})

// History controller
.controller('HistoryCtrl', function($scope, Trips, Profil) {

  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      var userId = Profil.get().uid;

      var records = [];

      Profil.getHistory(userId).orderByKey().once('value').then(function(snapshot) {

          snapshot.forEach(function(childSnapshot) {
              // key will be "ada" the first time and "alan" the second time
              var key = childSnapshot.key;
              // childData will be the actual contents of the child
              var childData = childSnapshot.val();
              records.push(childData);
          });
          $scope.records = records;
      });
    }
  });

  /* $scope.records = Trips.all(); */


  // FONCTION pour enregistrer un nouvel échange
  // Trips.save();
})

.controller('IntroCtrl', function($scope, $rootScope, $state, $ionicSlideBoxDelegate) {

  // Called to navigate to the main app
  $scope.startApp = function() {
    $state.go('home');
  };
  $scope.next = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.previous = function() {
    $ionicSlideBoxDelegate.previous();
  };

  // Called each time the slide changes
  $scope.slideChanged = function(index) {
    $scope.slideIndex = index;
  };
})

// Notification controller
.controller('NotificationCtrl', function($scope, Notifications) {


  // get list of notifications from model
  $scope.notifications = Notifications.all();
})

// Support controller
.controller('SupportCtrl', function($scope, $cordovaEmailComposer) {
/*
  $cordovaEmailComposer.isAvailable().then(function() {
     // is available
     alert("available");
   }, function () {
     // not available
     alert("not available");
   });
   $scope.sendEmail = function(){
    var email = {
       to: 'pierredrege_2@hotmail.com',
       cc: 'teste@example.com',
       bcc: ['john@doe.com', 'jane@doe.com'],
       attachments: [
         'file://img/logo.png',
         'res://icon.png',
         'base64:icon.png//iVBORw0KGgoAAAANSUhEUg...',
         'file://README.pdf'
       ],
       subject: 'Mail subject',
       body: 'How are you? Nice greetings from Leipzig',
       isHtml: true
    };

   $cordovaEmailComposer.open(email).then(null, function () {
     // user cancelled email
    });
   }
*/
})

.controller('cguCtrl', function($scope) {

$scope.cgu2 = true;

})

.controller('MdpOublieCtrl', function($scope, $state, $ionicPopup) {

  $scope.mdpOublie = function() {
    var auth = firebase.auth();
    var emailAddress = document.getElementById('email_mdpo').value;

      auth.sendPasswordResetEmail(emailAddress).then(function() {
        // Email sent.
        var alertPopup = $ionicPopup.alert({
          title: 'Succès',
          template: 'Votre nouveau mot de passe vous a été envoyé sur votre adresse email'
        });
      }, function(error) {
        // An error happened.
        var alertPopup = $ionicPopup.alert({
          title: 'Désolé',
          template: 'Une erreur s\'est produite lors du traitement de votre demande'
        });
      });


    return false;
  };
})

// Profile controller
.controller('ProfileCtrl', function($scope, Profil, Voitures, Couleurs) {

  $scope.voitures = Voitures.get();
  $scope.couleurs = Couleurs.get();

  $scope.selectedOption = $scope.voitures[0].model;

  firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        var userId = Profil.get().uid;

        $scope.changerMdp = function(mdp) {
          user.updatePassword(mdp);
        }

        Profil.getPrenom(userId).once('value').then(function(snapshot) {
          username = snapshot.val().prenom;
          $scope.prenom = username;
        });

        $scope.email = Profil.get().email;
      }
  });




 // user data
    $scope.user = {
      name: "Adam Lambert",
      profile_picture: "img/thumb/adam.jpg",
      phone: "Citroën",
      email: "success.ddt@gmail.com"
    }
})

// Authentication controller
// Put your login, register functions here
.controller('AuthCtrl', function($scope, $ionicHistory, $state, $ionicPopup, $location, Voitures, Couleurs, Profil, $ionicModal, API, $ionicLoading) {
  // hide back butotn in next view
  $ionicHistory.nextViewOptions({
    disableBack: true
  });

// ----------------------- INSCRIPTION -------------------------------
$scope.voitures = Voitures.get();
$scope.couleurs = Couleurs.get();

// -- CGU --


$ionicModal.fromTemplateUrl('contact-modal.html', {
  scope: $scope,
  animation: 'slide-in-up'
}).then(function(modal) {
  $scope.modal = modal
})

$ionicModal.fromTemplateUrl('cgu-modal.html', {
  scope: $scope,
  animation: 'slide-in-up'
}).then(function(modal) {
  $scope.cgu = modal
})



var openModal = function(voitureModel, voitureCouleur, prenom, email_inscription, mdp_inscription) {
  $scope.modal.show();
  $scope.voitureModel = voitureModel;
  $scope.voitureCouleur = voitureCouleur;
  $scope.prenom = prenom;
  $scope.email_inscription = email_inscription;
  $scope.mdp_inscription = mdp_inscription;
}

$scope.goCgu = function() {
  $scope.cgu.show();
}
$scope.closeCgu = function() {
  $scope.cgu.hide();
}

$scope.closeModal = function(voitureModel, voitureCouleur, prenom, email_inscription, mdp_inscription) {
  $scope.modal.hide();
  Profil.inscription(email_inscription, mdp_inscription).then(function() {
    var userId = Profil.get().uid;

    if (userId != null && voitureModel != "Voiture" && voitureCouleur != "Couleur" && prenom != "") {
      Profil.save(userId, prenom, voitureModel, voitureCouleur);
      user= {} ;
      user.uid= userId;
      user.prenom= prenom;
      user.email= email_inscription ;
      var tokenStore = window.sessionStorage
      tokenStore['fbtoken'] = userId;
      console.log("windowStorage" + window.sessionStorage['fbtoken']);
      user.token=tokenStore['fbtoken'] ;
      user.deviceToken= tokenStore['fbtoken'] ;
      saveInBackend(user) ;
      $state.go('inscription-reussie');
    }
    else {
      var user = Profil.get();

      user.delete().then(function() {
        // User deleted.
        var alertPopup = $ionicPopup.alert({
          title: 'Erreur',
          template: 'Veuillez remplir tous les champs.'
        });
      }, function(error) {
        var alertPopup = $ionicPopup.alert({
          title: 'Erreur',
          template: 'user.delete() n\'a pas fonctionné'
        });
      });
    }
  }, function(error) {
      switch(error.code) {
        case "auth/email-already-in-use":
          var alertPopup = $ionicPopup.alert({
            title: 'Désolé',
            template: 'Il existe déjà un compte avec cet email.'
          });
          break;
        case "auth/invalid-email":
          var alertPopup = $ionicPopup.alert({
            title: 'Désolé',
            template: 'L\'adresse email n\'est pas valide.'
          });
          break;
        case "auth/operation-not-allowed":
          var alertPopup = $ionicPopup.alert({
            title: 'Erreur',
            template: 'Veuillez activer la connexion email/mdp dans firebase.'
          });

          break;
        case "auth/weak-password":
          var alertPopup = $ionicPopup.alert({
            title: 'Désolé',
            template: 'Votre mot de passe doit contenir au moins 6 caractères.'
          });
          break;
      }

  });
};

$scope.closeModal2 = function() {
  $scope.modal.hide();
  showAlert();
};

var showAlert = function() {
 var alertPopup = $ionicPopup.alert({
   title: 'Erreur',
   template: 'Vous devez accepter les conditions générales d\'utilisation.'
 });
};

$scope.$on('$destroy', function() {
 $scope.modal.remove();
})


    function saveInBackend(user) {
      console.log(angular.toJson(user));
      console.log("Sending request to backend");
      params = {
        "facebook_id": user.uid,
        "token": window.sessionStorage['fbtoken'],
        //"email": user.email,
        "email": user.email,
        "first_name": user.prenom,
        "last_name": user.prenom,
        //"gender": user.gender
        "gender": "H",
        "device_token": window.sessionStorage['fbtoken'],
        "mixpanel_id": window.sessionStorage['fbtoken']

      };

      console.log("params" + params.facebook_id);
      API.post('/api/v1/users', params).
        success(function(data, status, headers, config) {
          // Redirect to the Park Screen
          // User.save(data["user"]);
          $ionicLoading.hide();
          //if (data["user"]["phone"]) {
          $state.go('home');
          //} else {
          // $state.go('phone');
          //}
        }).
        error(function(data, status, headers, config) {
          // called asynchronously if an error occurs
          console.log("erreur");
          // or server returns response with an error status.
        });
    };



    $scope.validerInscription =
    function(voitureModel, voitureCouleur, prenom, email_inscription, mdp_inscription) {
            var cguValide = openModal(voitureModel, voitureCouleur, prenom, email_inscription, mdp_inscription);

   return false;
};
// ----------------------- CONNEXION -------------------------------
$scope.validerConnexion = function (email,mdp) {
   var tokenStore = window.sessionStorage
   Profil.connexion(email,mdp).then(function() {

     tokenStore['fbtoken'] = Profil.get().uid;
     console.log("windowStorage" + window.sessionStorage['fbtoken']);

     $scope.user = Profil.get();
     console.log('User name is ' + Profil.getPrenom());
     $scope.myChannel = 'sweetch-' + $scope.user.uid;
     $state.go('home');

   }, function(error) {

     switch(error.code) {
       case "auth/user-disabled":
         var alertPopup = $ionicPopup.alert({
           title: 'Désolé',
           template: 'Ce compte a été désactivé.'
         });
         break;
       case "auth/invalid-email":
         var alertPopup = $ionicPopup.alert({
           title: 'Désolé',
           template: 'L\'adresse email n\'est pas valide.'
         });
         break;
       case "auth/user-not-found":
         var alertPopup = $ionicPopup.alert({
           title: 'Désolé',
           template: 'L\'email est inconnu.'
         });

         break;
       case "auth/wrong-password":
         var alertPopup = $ionicPopup.alert({
           title: 'Désolé',
           template: 'Le mot de passe est incorrecte.'
         });
         break;
     }
   });
   return false;
};

})
